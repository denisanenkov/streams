package ru.anenkov.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamClass {

    /**
     * * * * * * * * * ** * * * * * * * * * * * * * * * *
     * Способы создания stream-объектов:
     * <p>
     * * * * * * * Stream Methods * * * * * * * * * * * * *
     * 1. Stream.of(element1, element2, .., elementN);
     * 2. Stream.builder.add(...).add(...).add(...);
     * 3. Stream.iterate(seed, iterator);
     * 4. Stream.generate(() -> element).limit(element_count);
     * <p>
     * * * * * * * * * From File * * * * * * * * * * * * * *
     * 5. Files.lines(Paths.get("file_path"));
     * <p>
     * * * * * * * * From Collection, Array * * * * * * * *
     * 6. collectionName.stream();
     * 7. arrayName.stream();
     * <p>
     * * * * * * * * From Int String * * * * * * * * * * *
     * 8. IntStream stream = "".chars;
     * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * <p>
     * * * * * * Parallel Stream From Collection * * * * *
     * 9. collection.parallelStream()
     */
    public void createStream() throws IOException {
//      1 - Создание из коллекции
        Collection<String> collection = Arrays.asList("a1", "a2", "a3");
        Stream<String> streamFromCollection = collection.stream();
//      2 - Создание из последовательности
        Stream<String> streamFromValues = Stream.of("1", "2", "3", "2");
//      3 - Создание из массива
        String[] array = {"123", "43", "543"};
        Stream<String> stringStreamFromArray = Arrays.stream(array);
//      4 - Создание из строки
        IntStream intStream = "123".chars();
//      5 - Сохдание из файла
        Stream<String> streamFromFile = Files.lines(Paths.get("test.txt"));
//      6 - StreamBuilder
        Stream.builder().add("a1").add("a2").add("a3").build();
//      7 - ParallelStream
        collection.parallelStream().limit(1).forEach(System.out::println);
//      8 - IterateStream
        Stream.iterate(4, n -> n + 10).limit(3).forEach(System.out::println);
//      9 - GenerateStream
        Stream.generate(() -> "a1").limit(3).forEach(System.out::println);
    }

    /**
     * Методы работы со Stream
     */
    public void methodsWithStream() {
        System.out.println("*** filter lambda ***");
        Stream.of("a1", "a2", "a3", "a4", "a1").filter(current -> current.contains("1")).forEach(System.out::println);

        System.out.println("*** filter contains ***");
        Stream.of("a1", "a2", "a3", "a4", "a1").filter("a3"::contains).forEach(System.out::println);

        System.out.println("*** distinct - Возвращает стрим без дубликатов (для метода equals) ***");
        Stream.of("a1", "a2", "a3", "a4", "a1").distinct().forEach(System.out::println);

        System.out.println("*** map - Преобразует каждый элемент стрима ***");
        Stream.of("a1", "a2", "a3", "a4", "a1").distinct().map((s) -> s + "_afterMapFunction")
                .collect(Collectors.toList()).forEach(System.out::println);

        System.out.println("*** peek - Применяет функцию к каждому элементу стрима ***");
        Stream.of("one", "two", "three", "four")
                .filter(e -> e.length() > 3)
                .peek(e -> System.out.println("Filtered value: " + e))
                .map(String::toUpperCase)
                .peek(e -> System.out.println("Mapped value: " + e))
                .collect(Collectors.toList());

        System.out.println("*** limit - Позволяет ограничить выборку определенным количеством первых элементов ***");
        List<String> list = Stream.of("a1", "a2", "a3", "a4", "a1").limit(3).collect(Collectors.toList());

        System.out.println("*** sorted - Применяет функцию сортировки к стриму ***");
        Stream.of("d6", "b5", "a1", "a2", "a3", "a4", "a1", "a0").sorted().map((s) -> s + "_sorted")
                .forEach(System.out::println);

        System.out.println("*** mapToInt, mapToDouble, mapToLong - Аналог map, но возвращает числовой стрим ***");
        Stream.of("a1", "a2", "a3", "a4", "a1").mapToInt((s) -> Integer.parseInt(s)).toArray();

    }

}
